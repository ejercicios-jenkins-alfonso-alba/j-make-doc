#!/usr/bin/env groovy

def call () {
    echo "Genera documentación"
    def command = 'make'
    def resultado = sh returnStatus: true, script: "${command} doc"
    if(resultado == 0) {
        sh "zip -r doxygen.zip html/"
        archiveArtifacts artifacts: 'doxygen.zip', followSymlinks: false
        publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'html', reportFiles: 'index.html', reportName: 'Doxygen documentación', reportTitles: ''])
    } else {
        error 'No pude generara la documentación'
    }

}
